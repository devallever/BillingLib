package app.allever.project.billing.demo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import app.allever.lib.billing.BillingConfig
import app.allever.lib.billing.BillingHelper
import app.allever.project.billing.demo.databinding.FragmentFirstBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            btnConnect.setOnClickListener {
                BillingHelper.connect()
            }

            btnDisConnect.setOnClickListener {
                BillingHelper.disConnect()
            }

            btnProductDetails.setOnClickListener {
                BillingHelper.getProductDetails(BillingConfig.PRODUCT_ID_LIST) { success, code, message ->
                    log("查询商品：$success")
                }
            }

            btnSubStatus.setOnClickListener {
                BillingHelper.checkScribeStatus { success, code, message ->
                    log("是否订阅：$success")
                    if (success) {
                        toast("已订阅")
                    } else {
                        toast("未订阅")
                    }
                }
            }

            btnSub.setOnClickListener {
                BillingHelper.subScribe(
                    requireActivity(),
                    BillingConfig.PRODUCT_WEEKLY
                ) { success, code, message ->
                    log("订阅：$success")
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun log(msg: String) {
        Log.d("ILogger", msg)
    }

    private fun toast(msg: String) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
    }
}