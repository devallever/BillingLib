package app.allever.project.billing.demo

import android.app.Application
import app.allever.lib.billing.BillingHelper
import app.allever.lib.billing.BillingV6

/**
 *@Description
 *@author: zq
 *@date: 2024/1/18
 */
class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        BillingHelper.init(this, BillingV6())
    }
}