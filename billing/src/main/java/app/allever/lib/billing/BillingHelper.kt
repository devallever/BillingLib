package app.allever.lib.billing

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Handler
import android.os.Looper

/**
 * 文档：
 * https://developer.android.com/google/play/billing/getting-ready?hl=zh-cn#products
 *
 * 接入文档
 * https://developer.android.com/google/play/billing/integrate?hl=zh-cn
 *
 * 创建订阅文档：
 * https://support.google.com/googleplay/android-developer/answer/140504?hl=zh-Hans
 *
 */
@SuppressLint("StaticFieldLeak")
object BillingHelper : IBilling {
    @SuppressLint("StaticFieldLeak")
    lateinit var context: Context
    val mainHandler by lazy {
        Handler(Looper.getMainLooper())
    }

    private var mBinning: IBilling? = null

    fun init( context: Context, billing: BaseBilling) {
        this.context = context.applicationContext
        mBinning = billing
        connect()
    }

    override fun connect() {
        mBinning?.connect()
    }

    override fun disConnect() {
        mBinning?.disConnect()
    }

    override fun getProductDetails(
        productIdList: MutableList<String>,
        finish: ((success: Boolean, code: Int, message: String) -> Unit)?
    ) {
        mBinning?.getProductDetails(productIdList, finish)
    }

    override fun subScribe(
        activity: Activity,
        productId: String,
        finish: (success: Boolean, code: Int, message: String) -> Unit
    ) {
        mBinning?.subScribe(activity, productId, finish)
    }

    override fun checkScribeStatus(finish: (success: Boolean, code: Int, message: String) -> Unit) {
//        finish.invoke(true, 0, "")
//        return
        mBinning?.checkScribeStatus(finish)
    }
}