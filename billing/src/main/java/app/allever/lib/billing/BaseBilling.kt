package app.allever.lib.billing

abstract class BaseBilling : IBilling {
    val DEFAULT_ERROR_MSG = "Un support the country"

    protected var mIsConnect = false
    protected var mNeedReconnect = false

    override fun disConnect() {
        log("disconnect google")
        mNeedReconnect = false
        mIsConnect = false
    }

    protected fun reconnectIfNeed() {
        if (mNeedReconnect) {
            BillingHelper.mainHandler.postDelayed({
                connect()
            }, 1000)
        }
    }
}

