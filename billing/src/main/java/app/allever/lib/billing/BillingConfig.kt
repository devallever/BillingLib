package app.allever.lib.billing

object BillingConfig {
    val PRODUCT_WEEKLY = "gif_memes_weekly"
    val PRODUCT_YEARLY = "gif_memes_yearly"

    val PRODUCT_ID_LIST = mutableListOf(PRODUCT_WEEKLY, PRODUCT_YEARLY)
}