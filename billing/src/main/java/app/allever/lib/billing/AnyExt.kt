package app.allever.lib.billing

import android.util.Log
import android.widget.Toast
import com.google.gson.Gson

/**
 *@Description
 *@author: zq
 *@date: 2024/1/18
 */

private val mGson by lazy {
    Gson()
}
fun Any.toJson(): String {
    return mGson.toJson(this)
}

internal fun toast(msg: String) {
    Toast.makeText(BillingHelper.context, msg, Toast.LENGTH_SHORT).show()
}

private const val TAG = "BillingHelper"
internal fun log(msg: String) {
    if (BuildConfig.DEBUG) {
        Log.d(TAG, msg)
    }
}

internal fun logE(msg: String) {
    if (BuildConfig.DEBUG) {
        Log.e(TAG, msg)
    }
}